//.fetch - way to retrieve data from db/server
let posts = fetch('https://jsonplaceholder.typicode.com/posts')
	//.then - waits for the data to be fulfilled before it moves on to other codes. It returns a promise that is consummable using the .then syntax as well.
	.then((response) => response.json())
	.then((json) => console.log(json));

/*console.log(posts);*/

//async and await - JS ES6 equivalent of the .then syntax. It allows us to write more streamlined/clean code while utilizing an API fetching functionality.
async function fetchPosts() {
	let result = await fetch('https://jsonplaceholder.typicode.com/posts');

	/*console.log(result);*/

	let json = await result.json();

	console.log(json);
};

fetchPosts();

//get single post from db/server
let post = fetch('https://jsonplaceholder.typicode.com/posts/10')
	.then((response) => response.json())
	.then((data) => console.log(data));

/*console.log(post);*/

//add single post from db/server
fetch('https://jsonplaceholder.typicode.com/posts', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		userId: 1,
		title: 'New Post',
		body: 'Hello World!'
	})
})
.then((response) => response.json())
.then((data) => console.log(data));

//update single post from db/server
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Updated Post',
	})
})
.then((response) => response.json())
.then((data) => console.log(data));

//delete single post from db/server
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'DELETE'
})
.then((response) => response.json())
.then((data) => console.log(data));

//filter post from db/server
fetch('https://jsonplaceholder.typicode.com/posts?userId=1')
.then((response) => response.json())
.then((data) => console.log(data));

//retrieve comments from post
fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
.then((response) => response.json())
.then((data) => console.log(data));